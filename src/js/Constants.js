/**
 * Created by AndrewForExample on 10.04.2016.
 */

const AU = 149600000000;
const LY = 9460730472580800;
module.exports = {

    AU: AU,

    PARSEC: LY *  3.26156,

    EARTH_RADIUS: 6371000,

    LIGHT_YEAR: LY,

    PLANETS: {
        MERCURY: planet(0.38),
        VENUS: planet(0.72),
        EARTH: planet(1),
        MARS: planet(1.52),
        JUPITER: planet(5.20),
        SATURN: planet(9.54),
        URANUS: planet(19.22),
        NEPTUN: planet(30.06),
        PLUTO: planet(39.2),
    }
};

function planet(r){
    return {
        ORBITAL_RADIUS: r * AU
    }
}
