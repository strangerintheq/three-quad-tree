var THREE = require('three');
var OrbitControls = require('three-orbit-controls')(THREE);
var WindowResize = require('three-window-resize');
var NoUiSlider = require('nouislider');

var QuadTreeEarth = require('./QuadTreeEarth');
var Atmosphere = require('./Atmosphere');
var Stats = require('./Stats');
var Stars = require('./Stars');
var Helpers = require('./Helpers');
var Solarsystem = require('./Solarsystem');

var renderer = createRenderer();
var camera = createCamera();
new WindowResize(renderer, camera);
var scene = new THREE.Scene();
createSlider();

var earth = QuadTreeEarth.create();
earth.add(Atmosphere.create(earth.radius + 150000));
//var sun = createSun();
var helpers = Helpers.create();

scene.add(new THREE.AmbientLight(new THREE.Color(0.7, 0.7, 0.7)));
scene.add(camera);
scene.add(earth);
//scene.add(sun);
scene.add(Stars.create());
scene.add(helpers);
scene.add(Solarsystem.create());

animate();

function animate() {
    Stats.fps.begin();
    requestAnimationFrame(animate);
    render();
    Stats.fps.end();
}

function render() {
    earth.update(camera);
    helpers.update(camera);
    renderer.render(scene, camera);
    Stars.update(camera, renderer);
}

function createRenderer(){
    var renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.sortObjects = false;
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);
    document.body.style.margin = '0';
    document.body.style.overflow = 'hidden';
    return renderer;
}

function createCamera() {
    var camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1000, Math.pow(10, 23));
    camera.position.z = 30000000;
    var controls = new OrbitControls(camera);
    controls.minDistance = 6500000;
    return camera;
}

function createSun() {
    var sun = new THREE.Object3D();
    //sun.add(new THREE.DirectionalLight(new THREE.Color(0.7, 0.7, 0.7)));
    sun.add(new THREE.AmbientLight(new THREE.Color(0.7, 0.7, 0.7)));
    sun.position.set(1, 0.5, 0).normalize();
    sun.position.multiplyScalar(149600000000);
    sun.add(new THREE.Mesh(new THREE.SphereBufferGeometry(696342000, 8, 8), new THREE.MeshBasicMaterial({
        color: new THREE.Color(1, 1, 1)
    })));
    return sun;
}

function createSlider(){
    var slider = document.createElement('div');
    slider.style.height = "95%";
    slider.style.margin = "20px";
    slider.style.marginRight = "10px";
    slider.style.zIndex = "100";
    slider.style.top = "0";
    slider.style.right = "0";
    slider.style.position = "absolute";
    document.body.appendChild(slider);
    NoUiSlider.create(slider, {
        start: 40,
        orientation: 'vertical',
        range: {
            'min': Math.pow(10, 8)/2,
            '20%': Math.pow(10, 8),
            '25%': Math.pow(10, 9),
            '30%': Math.pow(10, 10),
            '35%': Math.pow(10, 11),
            '40%': Math.pow(10, 12),
            '45%': Math.pow(10, 13),
            '50%': Math.pow(10, 14),
            '55%': Math.pow(10, 15),
            '60%': Math.pow(10, 16),
            '65%': Math.pow(10, 17),
            '70%': Math.pow(10, 18),
            '75%': Math.pow(10, 19),
            '80%': Math.pow(10, 20),
            '85%': Math.pow(10, 21),
            '90%': Math.pow(10, 22),
            '95%': Math.pow(10, 23),
            'max': Math.pow(10, 24)
        }
    });

    slider.noUiSlider.on('update', function(){
        var s = slider.noUiSlider.get();
        camera.position.normalize().multiplyScalar(s);
    });
}
