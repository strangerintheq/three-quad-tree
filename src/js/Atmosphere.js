var THREE = require('three');

var vertexShader = [
    'varying vec3 vNormal;',
    'void main(){',
    '	vNormal		= normalize( normalMatrix * normal );',
    '	gl_Position	= projectionMatrix * modelViewMatrix * vec4( position, 1. );',
    '}'
].join('\n');

var fragmentShader = [
    'uniform float coeficient;',
    'uniform float power;',
    'uniform vec3  glowColor;',

    'varying vec3  vNormal;',

    'void main(){',
    '	float intensity	= pow( coeficient - dot(vNormal, vec3(0.0, 0.0, 1.0 )), power );',
    '	gl_FragColor	= vec4( glowColor * intensity*2., 1.6 );',
    '}'
].join('\n');

module.exports = {
    create: function(radius) {
        return new THREE.Mesh(
            new THREE.SphereBufferGeometry(radius, 64, 32),
            new THREE.ShaderMaterial({
                uniforms: {
                    coeficient: { type: "f", value: 1.0 },
                    power: { type: "f", value: 3.0},
                    glowColor: {type: "c", value: new THREE.Color(0.5, 0.8, 1.2)}
                },
                vertexShader: vertexShader,
                fragmentShader: fragmentShader,
                side: THREE.FrontSide,
                blending: THREE.AdditiveBlending,
                transparent: true,
                depthWrite: false
            })
        );
    }
};
