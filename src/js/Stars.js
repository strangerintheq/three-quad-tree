/**
 * Created by AndrewForExample on 03.04.2016.
 */
const THREE = require('three');
const Constants = require('./Constants');
const Stats = require('./Stats');
const loader = new THREE.TextureLoader();
const frustum = new THREE.Frustum();
const namedGeometry = new THREE.Geometry();
const stars = new THREE.Object3D();
stars.rotateX(-Math.PI/2);

const labels = document.createElement('div');
document.body.appendChild(labels);

module.exports = {

    create: function() {
        loadStarData(onLoad);
        return stars;
    },

    update: function(camera, renderer) {
        namedGeometry.vertices.forEach(function(star) {
            updateStar(star, camera, renderer);
        });
    }
};

function updateStar(star, camera, renderer) {
    var vector = star.clone();
    vector.applyMatrix4(stars.matrixWorld);

    var d = camera.position.length();
    var r = Constants.EARTH_RADIUS;
    var fov = camera.fov / 2 * Math.PI / 180.0;
    var projectedEarthRadius = 1.0 / Math.tan(fov) * r / Math.sqrt(d * d - r * r);

    projectedEarthRadius *= renderer.domElement.height/2;

    frustum.setFromMatrix( camera.projectionMatrix.clone().multiply(camera.matrixWorldInverse) );
    var visible = frustum.containsPoint(vector);

    var canvas = renderer.domElement;
    vector.project( camera );
    var x = Math.round( ( vector.x + 1 ) * canvas.width / 2 );
    var y = Math.round( ( 1 - vector.y ) * canvas.height / 2 );

    if (Math.pow(canvas.width/2 - x, 2) + Math.pow(canvas.height/2 - y, 2) < Math.pow(projectedEarthRadius, 2)) {
        visible = false;
    }

    star.label.update(visible, x, y);
}

function onLoad(result) {
    var len = result.stars.length;
    var geometry = new THREE.BufferGeometry();
    var positions = new Float32Array( len * 3 );
    var colors = new Float32Array( len );
    var sizes = new Float32Array( len );
    var max = 0;
    var min = 0;
    for (var i = 0; i < len; i++) {
        var star = result.stars[i];
        var data = getStarData(star);
        positions[ i*3 ] = data.distance > 9 * 1000000 * Constants.PARSEC ? 0 : data.x;
        positions[ i*3 + 1 ] = data.distance > 9 * 1000000 * Constants.PARSEC ? 0 : data.y;
        positions[ i*3 + 2 ] = data.distance > 9 * 1000000 * Constants.PARSEC ? 0 : data.z;
        colors[i] = data.color;
        sizes[i] = data.distance < 20 * Constants.LIGHT_YEAR ? 1.5 : 0.5;
        if (star.name) {
            sizes[i] = 4.0;
            var starVertex = new THREE.Vector3(data.x, data.y, data.z);
            namedGeometry.vertices.push(starVertex);
            //console.log(star);
            //stars.add(createStar(data));
            starVertex.label = createLabel(star);
            labels.appendChild(starVertex.label);
        }

        if (star.c > max) max = star.c;
        if (star.c < min) min = star.c;

    }
    console.log(min + " - " + max);

    geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
    geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 1 ) );
    geometry.addAttribute( 'size', new THREE.BufferAttribute( sizes, 1 ) );
    stars.add(new THREE.Points(geometry,
        //new THREE.PointsMaterial()
        createMaterial()
    ));
    stars.add(new THREE.Points(namedGeometry, new THREE.PointsMaterial()))

}

function createLabel(star){
    var label = document.createElement('span');
    label.innerHTML = star.name.toUpperCase();
    label.classList.add('star-label');
    label.update = function(visible, x, y) {
        label.style.display = visible ? 'block' : 'none';
        label.style.top = y + 'px';
        label.style.left = x + 'px';
    };
    return label;
}

function map( value, istart, istop, ostart, ostop) {
    return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

function createStar(starData) {
    var mesh = new THREE.Mesh(new THREE.SphereBufferGeometry(100000000000000, 8, 8), new THREE.MeshBasicMaterial({
        color: new THREE.Color(1, 1, 1)
    }));
    mesh.position.set(starData.x, starData.y, starData.z);
    return mesh;
}

function loadStarData(callback) {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', function() {
        var parsed = JSON.parse(xhr.responseText);
        if(callback){
            callback(parsed);
        }
    }, false);
    xhr.open('GET', 'stars.json', true);
    xhr.send(null);
}

function getStarData(star) {
    //	original data is in parsecs
    var distance = star.d * 3.26156 * Constants.LIGHT_YEAR;
    //	http://math.stackexchange.com/questions/52936/plotting-a-stars-position-on-a-2d-map
    var phi = (star.ra+90) * 15 * Math.PI/180;
    var theta = star.dec * Math.PI/180;
    var rho = distance;
    var rVec = rho * Math.cos( theta );
    return {
        x: rVec * Math.cos( phi ),
        y: rVec * Math.sin( phi ),
        z: rho * Math.sin( theta ),
        distance: distance,
        color: map(star.c, -0.3, 1.52, 0, 1)
    }
}

function createMaterial1() {

    var datastarTexture0 = THREE.ImageUtils.loadTexture("images/p_0.png" );
    var datastarTexture1 = THREE.ImageUtils.loadTexture("images/p_2.png" );
    var datastarHeatVisionTexture = THREE.ImageUtils.loadTexture( "images/sharppoint.png");
    var starColorGraph = THREE.ImageUtils.loadTexture('images/star_color_modified.png');

    var datastarUniforms = {
        color: { type: "c", value: new THREE.Color( 0xffffff ) },
        texture0: { type: "t", value: datastarTexture0 },
        texture1: { type: "t", value: datastarTexture1 },
        heatVisionTexture: { type: "t", value: datastarHeatVisionTexture },
        spectralLookup: {type: "t", value: starColorGraph },
        idealDepth: { type: "f", value: 1.0 },
        blurPower: { type: "f", value: 2.0 },
        blurDivisor: { type: "f", value: 2.0 },
        sceneSize: { type: "f", value: 12.0 },
        cameraDistance: { type: "f", value: 800.0 },
        zoomSize: { type: "f", value: 100.0 },
        scale: { type: "f", value: 1.0 },
        brightnessScale: { type: "f", value: 1.0 },
        heatVision: { type: "f", value: 0.0 }
    };

    var vertex = [
        'uniform float zoomSize;',
        'uniform float scale;',
        'attribute float size;',
        'attribute vec3 customColor;',
        'attribute float colorIndex;',

        'varying vec3 vColor;',
        'varying float dist;',
        'varying float pSize;',
        'varying float zoom;',
        'varying float starColorLookup;',

        'void main() {',
        '    vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );',

        '    vColor = customColor;',
        '    dist = length( mvPosition.xyz );',
        '    pSize = gl_PointSize;',
        '    zoom = zoomSize;',
        '    starColorLookup = colorIndex;',

        '    gl_PointSize = scale * size  * zoomSize / dist;',
        '    gl_Position = projectionMatrix * mvPosition;',

        '}'
    ].join("\n");

    var fragment = [
        'uniform vec3 color;',
        'uniform sampler2D texture0;',
        'uniform sampler2D texture1;',
        'uniform sampler2D spectralLookup;',
        'uniform sampler2D heatVisionTexture;',
        'uniform float idealDepth;',
        'uniform float blurPower;',
        'uniform float blurDivisor;',
        'uniform float sceneSize;',
        'uniform float cameraDistance;',
        'uniform float brightnessScale;',
        'uniform float heatVision;',
        'varying vec3 vColor;',
        'varying float dist;',
        'varying float zoom;',
        'varying float starColorLookup;',
        'void main() {',
        '   gl_FragColor = vec4( color * vColor, 1. );',

        '   float depth = gl_FragCoord.z / gl_FragCoord.w;',
        '   depth = 1. - (depth / sceneSize );',
        '   float focus = clamp( depth, 0., 1. );',
        '   vec4 color0 = texture2D(texture0, gl_PointCoord );',
        '   vec4 color1 = texture2D(texture1, gl_PointCoord );',
        '   float clampedLookup = clamp( starColorLookup, 0., 1.0 );',
        '   vec2 spectralUV = vec2( 0., clampedLookup );',
        '   vec4 starSpectralColor = texture2D( spectralLookup, spectralUV );',
        '   vec4 diffuse = mix( color1, color0, focus );',
        '   starSpectralColor.x = pow(starSpectralColor.x,2.);',
        '   starSpectralColor.y = pow(starSpectralColor.y,2.);',
        '   starSpectralColor.z = pow(starSpectralColor.z,2.);',
        '   diffuse.xyz *= starSpectralColor.xyz;',
        //'   vec3 blueTarget = (vec3(1.0) - vec3( 0.5, 0.6, .65)) * cameraDistance;',
        //'   diffuse.xyz -= blueTarget * (1.0-heatVision);',
        '   vec4 heatVisionTextureColor = texture2D( heatVisionTexture, gl_PointCoord );',
        '   vec4 heatVisionMixColor = mix( diffuse, starSpectralColor, heatVision );',
        '   if( heatVision > 0.0 && heatVisionTextureColor.w < 0.5 ){',
        '       discard;',
        '   }',
        '   gl_FragColor = heatVisionMixColor;',
        '}'
    ].join("\n");
    return new THREE.ShaderMaterial( {
        uniforms: datastarUniforms,
        vertexShader: vertex,
        fragmentShader: fragment,
        blending: THREE.AdditiveBlending,
        depthTest: false,
        depthWrite: false,
        transparent: true
    });
}


function createMaterial() {

    var uniforms = {
        texture: {type: "t", value: loader.load('images/sharppoint.png')}
    };

    return new THREE.ShaderMaterial({
        transparent: true,
        alphaTest: 0.5,
        blending: THREE.NormalBlending,
        depthWrite: false,
        depthTest: true,
        uniforms: uniforms,
        vertexShader: getVertexShader().join("\n"),
        fragmentShader: getFragmentShader().join("\n")
    });
}

function getVertexShader() {
    return [
        'attribute float color;',
        'attribute float size;',
        'varying float vColor;',
        'void main() {',
        '    vColor = color;',
        '    vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );',
        '    gl_PointSize = size; // length( mvPosition.xyz );',
        '    gl_Position = projectionMatrix * mvPosition;',
        '}'
    ];
}

function getFragmentShader() {
    return [
        'uniform sampler2D texture;',
        'varying float vColor;',
        'void main() {',
        '    vec2 p = vec2(gl_PointCoord.x, 1.0 - gl_PointCoord.y);', // flip texture
        '    gl_FragColor = texture2D(texture, vec2(p.x, p.y));',
        '}'
    ];
}