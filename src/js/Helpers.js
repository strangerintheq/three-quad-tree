/**
 * Created by kbalashev on 04.04.2016.
 */
var THREE = require('three');

const AU = 149597870700;
const LY = 9460730472580800;
const PC = LY * 3.26156;

module.exports = {
    create: function(){
        var helpers = new THREE.Object3D();
        helpers.add(createHelper(AU, "1 AU"));
        helpers.add(createHelper(AU*130, "130 AU, Voyager-I"));
        helpers.add(createHelper(LY, "1 LY"));
        helpers.add(createHelper(PC, "1 PC"));
        helpers.add(createHelper(PC*1000, "1 kPC"));
        helpers.add(createHelper(PC*1000*1000, "1 mPC"));
        helpers.update = function(camera) {
            var distance = camera.position.length();
            helpers.children.forEach(function(helper) {
                helper.update(distance);
            });
        };
        return helpers;
    }
};


function createHelper(len, text) {

    var pip = len / 50;

    var helper = new THREE.Object3D();

    var left = line(len, [
        [0, pip],
        [0, pip*2],
        [len/3, pip*2]
    ]);

    var right = line(len,[
        [len*2/3, pip*2],
        [len, pip*2],
        [len, pip]
    ]);

    helper.add(left);
    helper.add(right);

    helper.update = function(distance) {
        left.update(distance);
        right.update(distance);
    };

    return helper;
}

function line(len, verticesPositions) {

    var material = new THREE.LineBasicMaterial({
        color: 0xffffff,
        opacity: 0,
        transparent:true
    });

    var geometry = new THREE.Geometry();

    verticesPositions.forEach(function(p) {
        geometry.vertices.push(new THREE.Vector3(0, p[1],p[0]));
    });

    var line = new THREE.Line(geometry, material);

    line.update = function(distance) {
        line.visible = distance > len && distance < len * 20;
        line.material.opacity = distance/len;
    };

    line.visible = false;

    return line;
}