/**
 * Created by AndrewForExample on 01.04.2016.
 */
var Stats = require('stats-js');

var fps = new Stats();
fps.setMode(0); // 0: fps, 1: ms
fps.domElement.style.position = 'absolute';
fps.domElement.style.left = '0px';
fps.domElement.style.top = '0px';

var info = document.createElement('div');
info.style.width = '100px';
info.style.height = '100px';
info.style.position = 'absolute';
info.style.top = '70px';
info.style.color = 'white';


var stats = document.createElement('div');
stats.style.position = 'absolute';
stats.appendChild(fps.domElement);
stats.appendChild(info);
stats.fps = fps;
stats.info = info;

document.body.appendChild(stats);

module.exports = stats;

