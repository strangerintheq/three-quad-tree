/**
 * Created by kbalashev on 30.03.2016.
 */

var THREE = require('three');

var loader = new THREE.TextureLoader();
//loader.setCrossOrigin('');

const radius = 6371000;
const mil = 1000000;
const lod = 6;

const axes = {
    x: new THREE.Vector3(-1, 0, 0),
    y: new THREE.Vector3(0, 1, 0)
};

module.exports = {
    create: function() {
        var tree = createTree();
        var earth = new THREE.Object3D();
        earth.radius = radius;
        earth.add(tree);
        earth.update = function(camera) {
            tree.update(camera);
        };
        return earth;
    }
};


function createTree() {
    var tree = new THREE.Object3D();
    tree.cache = [];
    tree.root = createNode(tree);
    fillQuadTreeNode(tree.root, lod, false);
    fillQuadTreeNode(tree.root, lod, true);
    tree.update = function(camera) {
        var position = camera.position.clone();
        var level = getLevelFromDistance(position.length() - radius);
        var coords = getCoordinatesFromCameraPosition(position, level);
        tree.children.forEach(function(chunk) {
            chunk.setVisible(false);
        });
        var node = tree.cache[level][coords.column][coords.row];
        if (node) {
            setQuadVisible(node);
            node.mesh.setVisible(true);
        } else {
            console.warn("node not found: " + level + "-"+ coords.column + ":" + coords.row);
        }
    };
    return tree;

    function setQuadVisible(node) {
        if (null == node.parent) return;
        node.mesh.setVisible(true);
        node.parent.children.forEach(function(child) {
            child.mesh.setVisible(child !== node);
        });
        setQuadVisible(node.parent);
    }

    function fillQuadTreeNode(parent, depth, isRight) {
        for (var i = 0; i < 4; i++) {
            var c = parent.column * 2 + Math.floor(i / 2);
            var r = parent.row * 2 + i % 2;
            if (isRight) r += Math.pow(2, parent.level+1);
            var node = createNode(tree, parent, r, c);
            if (parent.level + 1 < depth) {
                fillQuadTreeNode(node, depth);
            }
            parent.add(node);
        }
        return parent;
    }
}

function createNode(tree, parent, row, column) {

    var level = parent == null ? 0 : parent.level + 1;
    row = row || 0;
    column = column || 0;

    var children = [];

    var textureName = "tiles/" + level + "/" + row + "/" + column + "." + (level < 4 ? "jpg" : "png");

    var chunk = new THREE.Object3D();
    chunk.setVisible = function(visible) {
        if (chunk.children.length == 0 && visible) {
            var material = new THREE.MeshPhongMaterial({
                //wireframe: true,
                color: color(random)
            });
            chunk.add(new THREE.Mesh(geometry(level, row, column), material));
            loader.load(textureName, function(texture) {
                material.map = texture;
                material.color = color(1.5);
                material.needsUpdate = true;
            });
        }
        // ugly looks, but works perfectly
        chunk.visible = visible;
        if (visible) {
            tree.add(chunk);
        } else {
            setTimeout(function() {
                tree.remove(chunk);
            }, 100);
        }
    };
    chunk.visible = false;


    var result = {

        children: children,
        level: level,
        row: row,
        column: column,
        parent: parent,
        mesh: chunk,

        add: function(chunk) {
            children.push(chunk);
        }
    };

    if (tree.cache[level] == null) tree.cache[level] = [];
    if (tree.cache[level][column] == null) tree.cache[level][column] = [];
    tree.cache[level][column][row] = result;

    return result;
}

function geometry(level, row, column) {
    var segments = 64 / Math.pow(2, level);
    if (level != 0) {
        var length = Math.PI / Math.pow(2, level);
        var phi = length * row;
        var theta = length * column;
    }
    return new THREE.SphereBufferGeometry(radius, segments, segments, phi, length, theta, length);

}

function getCoordinatesFromCameraPosition(position, level) {
    var result = {
        row: 0, column: 0
    };
    var size = 180/Math.pow(2, level);
    result.column = getCoordinate(position, axes.y, size);
    position.y = 0;
    result.row = getCoordinate(position, axes.x, size);
    if (position.z < 0 && level > 0) {
        result.row = Math.floor(Math.pow(2, level)*2 - result.row - 1);
    }
    return result;
}


function getCoordinate(v1, v2, size) {
    return Math.floor(angleBetweenVectors(v1, v2) / size);
}

// v2 must be normalized
function angleBetweenVectors(v1, v2) {
    var cos = v1.normalize().dot(v2);
    return THREE.Math.radToDeg(Math.acos(cos));
}

function getLevelFromDistance(d) {
    var level = 0;
    d /= mil*2;
    if (d < 9) level = 1;
    if (d < 5) level = 2;
    if (d < 4) level = 3;
    if (d < 2) level = 4;
    if (d < 1) level = 5;
    if (d < 0.5) level = 6;
    return level;
}

function color(v) { // value or function
    if (typeof v == "function") {
        return new THREE.Color(v(), v(), v());
    }
    return new THREE.Color(v, v, v);
}

function random() {
    return Math.random() + 0.5;
}
