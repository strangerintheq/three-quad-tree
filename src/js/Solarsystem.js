/**
 * Created by AndrewForExample on 10.04.2016.
 */
const Constants = require('./Constants');
const THREE = require('three');

module.exports = {
    create: function() {
        var mesh = new THREE.Object3D();
        //for (var planet in Constants.PLANETS) {
        //    if (Constants.PLANETS.hasOwnProperty(planet)) {
        //        mesh.add(createOrbit(Constants.PLANETS[planet]));
        //    }
        //}
        mesh.add(elliptic());
        mesh.translateZ(Constants.PLANETS.EARTH.ORBITAL_RADIUS);
        return mesh;
    }
};

function createOrbit(planet) {
    var geometry = new THREE.CircleGeometry(planet.ORBITAL_RADIUS, 256);
    geometry.vertices.shift();
    geometry.rotateX(Math.PI/2);
    return line(geometry);
}

function line(geometry) {
    var material = new THREE.LineBasicMaterial( { color: 0xffffff } );
    return new THREE.Line(geometry, material);
}

// http://stackoverflow.com/a/26050835
function elliptic() {

    var distanceFromSun = Constants.AU;
    var orbitScaleFactor = 1;
    var inclination = THREE.Math.degToRad(7.155);
    var semiMinorAxis = 1.00000261 * Constants.AU;
    var lonOfAscendingNode = THREE.Math.degToRad(348.73936);

    var x = distanceFromSun * orbitScaleFactor;
    var y = Math.sin(inclination) * distanceFromSun * orbitScaleFactor;
    var z = semiMinorAxis * orbitScaleFactor;

    var geometry = new THREE.Geometry();

    for (var i = 0; i < 2 * Math.PI; i += Math.PI / 64) {
        var vertex = new THREE.Vector3();
        vertex.x = Math.cos(i) * x;
        vertex.y = Math.cos(i + lonOfAscendingNode) * y;
        vertex.z = Math.sin(i) * z;
        geometry.vertices.push(vertex);
    }

    return line(geometry);
}
