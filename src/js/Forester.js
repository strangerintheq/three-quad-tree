// simple shader point cloud trees

var THREE = require('three');
var loader = new THREE.TextureLoader();

module.exports = {

    create: function(props) {
        var geometry = createGeometry(props.points, props.types);
        var material = createMaterial(props.types);
        var forest = new THREE.Points(geometry, material);
        forest.setLightPosition = function(position) {
            material.uniforms.light = position.clone().normalize();
        };
        return forest;
    },

    createType: function(textureName) {
        return {
            map: loader.load(textureName),
            scale: {
                value: 50,
                spread: 10
            }
        }
    },

    createRandomPoints: function(count, x, y, z) {
        var points = [];
        for (var i=0; i<count; i++){
            points.push(new THREE.Vector3(rand(x), rand(y), rand(z)));
        }
        return points;
    }
};

function rand(spread) {
    var sign = Math.random() > 0.5 ? 1 : -1;
    return Math.random() * spread * sign;
}

function createGeometry(points, props) {
    var geometry = new THREE.BufferGeometry();

    var vertices = new Float32Array( points.length * 3 );
    var types = new Float32Array( points.length );
    var scales = new Float32Array( points.length );

    for ( var i = 0; i < points.length; i++ ) {
        vertices[ i*3 ] = points[i].x;
        vertices[ i*3 + 1 ] = points[i].y;
        vertices[ i*3 + 2 ] = points[i].z;
        var typeIndex = Math.floor(props.length * Math.random());
        types[i] = typeIndex;
        var currentProps = props[typeIndex];
        scales[i] = currentProps.scale.value + currentProps.scale.spread * Math.random();
    }

    geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
    geometry.addAttribute( 'type', new THREE.BufferAttribute( types, 1 ) );
    geometry.addAttribute( 'scale', new THREE.BufferAttribute( scales, 1 ) );
    return geometry;
}

function createMaterial(types) {

    var uniforms = {
        textures: { type: "tv", value: [] },
        //normals: { type: "tv", value: [] },
        opacity: { type: "f", value: 0.5 },
        light: { type: "v3", value: new THREE.Vector3() }
    };

    types.forEach(function(type) {
        uniforms.textures.value.push(type.map);
        //uniforms.normals.value.push(type.normal);
    });

    return new THREE.ShaderMaterial({
        transparent: true,
        alphaTest: 0.5,
        blending: THREE.NormalBlending,
        depthWrite: false,
        depthTest: true,
        uniforms: uniforms,
        vertexShader: getVertexShader().join("\n"),
        fragmentShader: getFragmentShader(types).join("\n")
    });
}

function getVertexShader() {
    return [
        'attribute float type;',
        'attribute float scale;',

        'varying float vFrame;',
        'varying float vType;',
        'varying float vRotation;',

        'void main() {',
        '    if ( length(cameraPosition - position) > 5000.0 ) { ',
        '        gl_Position = vec4(2.0, 0.0, 0.0, 1.0);',  // discard 'too far' points
        '    } else {',
        '        vType = type;',

        '        vec3 v = normalize( cameraPosition - position );',
        '        float sinus = sqrt( v.x*v.x + v.z*v.z ) / length( v );',
        '        vFrame = floor(degrees(asin(sinus)) * 0.177777777);',

        '        vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );',
        '        gl_PointSize = scale * 100.0 / length( mvPosition.xyz );',
        '        gl_Position = projectionMatrix * mvPosition;',
        '    }',
        '}'
    ];
}

function getFragmentShader(types) {
    var l = types.length;
    return [
        'uniform sampler2D textures[' + l + '];',
        //'uniform sampler2D normals[' + l + '];',
        'uniform float opacity;',
        'uniform vec3 light;',

        'varying float vFrame;',
        'varying float vType;',
        'varying float vRotation;',

        'void main() {',

        '    vec2 point = vec2(gl_PointCoord.x, 1.0 - gl_PointCoord.y);', // flip texture

        '    float y = floor(vFrame * 0.25 + 0.01);', // frame x
        '    float x = floor(vFrame - y * 4.0 + 0.01);', // frame y

        '    vec2 offset = vec2(0.75 - x * 0.25, y * 0.25);',
        '    point.x = point.x * 0.25 + offset.x;',
        '    point.y = point.y * 0.25 + offset.y;',

        '    vec2 center = vec2(offset.x + 0.125, offset.y + 0.125);',
        '    float rx = cos(vRotation) * (point.x - center.x) + sin(vRotation) * (point.y - center.y) + center.x;',
        '    float ry = cos(vRotation) * (point.y - center.y) - sin(vRotation) * (point.x - center.x) + center.y;',

        '    for ( int i = 0; i < ' + l + '; i++ ) {',
        '        if ( i == int(vType + 0.01) ) {',
        '            vec4 color = texture2D( textures[i], vec2(rx, ry) );',
        '            if (opacity < color.w) {',
        '                color.w = opacity;',
        '            }',
        '            gl_FragColor = color;',
        '        }',
        '    }',
        '}'
    ];
}
