var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var concatCss = require('gulp-concat-css');
var minifyCss = require('gulp-cssnano');
var minifyHtml = require("gulp-htmlmin");

var webserver = require('gulp-webserver');

var tasks = [];
var outputFolder = "./build/";

task('js', 'src/js/**', function() {
    return browserify('src/js/Demo.js').bundle()
        //.pipe(uglify)
        .pipe(source('app.min.js'))
        .pipe(gulp.dest(outputFolder));
});

task('css', 'src/css/**', function() {
    return gulp.src('src/css/**')
        .pipe(concatCss("app.min.css"))
        .pipe(minifyCss())
        .pipe(gulp.dest(outputFolder));
});

task('html', 'src/html/**', function() {
    return gulp.src('src/html/**')
        .pipe(minifyHtml())
        .pipe(gulp.dest(outputFolder));
});

task('data', 'resources/**', function() {
    return gulp.src('resources/**')
        .pipe(gulp.dest(outputFolder));
});


gulp.task('default', tasks, function() {
    gulp.src(outputFolder).pipe(webserver({
        port: 8080, host: "0.0.0.0"
    }));
});


function task(name, watcherPattern, func) {
    gulp.task(name, func);
    if (null != watcherPattern) {
        gulp.watch(watcherPattern, function() {
            gulp.start(name);
        });
    }
    tasks.push(name);
}


