/**
 * Created by AndrewForExample on 04.04.2016.
 */
const start = new Date().getTime();
const MB = 1024*1024;
const fs = require('fs');
const PNG = require('pngjs').PNG;
const SPACE = '                                                ';
const A = 'A'.charCodeAt(0);

var fileFullPath, columns, rows, logProgress = 1;

try {
    fileFullPath = arg(2, "file");
    columns = arg(3, "column count");
    rows = arg(4, "row count");
} catch (e) {
    log(e.message, true);
    return;
}

const fileName = fileFullPath.substring(fileFullPath.lastIndexOf("\\") + 1);
const workPath = fileFullPath.substring(0, fileFullPath.lastIndexOf("\\"));

try {
    var size = processFile();
    log(fileName + ' - done, size: ' + size + ', execution time: ' + (new Date().getTime() - start)/1000 + 's');
} catch (e) {
    log(fileName + ' - ' + e.message, true);
}

function processFile() {
    var splitted = fileName.split('.');
    if (splitted.length != 5) {
        throw new Error(log("incorrect filename format: " + fileName));
    }
    return split();
}

function split() {
    var size = Math.floor(fs.statSync(fileFullPath).size/MB);
    log("reading " + size + "MB file...", true);
    var data = fs.readFileSync(fileFullPath);
    var png = PNG.sync.read(data, {
        filterType: -1
    });
    for (var c = 0; c < columns; c++) {
        for (var r = 0; r < rows; r++) {
            var filename = getFilename(fileName, c, r);
            log("processing: " + (c*rows + r + 1) + " of " + columns*rows, true);
            var tile = createTile(png, c, r);
            var buffer = PNG.sync.write(tile);
            fs.writeFileSync(filename, buffer);
        }
    }
    return size;
}

function createTile(src, column, row) {
    var dest = new PNG({
        filterType: -1,
        width: src.width/columns,
        height: src.height/rows
    });
    PNG.bitblt(src, dest, column*dest.width, row*dest.height, dest.width, dest.height, 0, 0);
    return dest;
}

function getFilename(file, c, r) {
    var segment = file.split('.');
    var row = (segment[3].charAt(1) - 1)*rows + r;
    var column = (segment[3].charCodeAt(0) - A)*columns + c;
    var path = workPath + "/" + segment[1];
    ensure(path);
    path += "/" + column;
    ensure(path);
    return path + "/" + row + ".png";
}

function ensure(dir) {
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
}

function progress() {
    logProgress++;
    if (logProgress > 3) {
        logProgress = 0;
    }
    switch (logProgress) {
        case 0: return "|";
        case 1: return "/";
        case 2: return "-";
        case 3: return "\\";
    }
}

function log(text, noNewLine) {
    if (noNewLine) {
        process.stdout.write(progress() + " " + text + SPACE + '\r');
    } else {
        console.log(text + SPACE);
    }
    return text;
}

function arg(index, name){
    if (!process.argv[index]) {
        throw new Error(name + " not specified");
    }
    return process.argv[index];
}